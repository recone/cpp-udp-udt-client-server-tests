//============================================================================
// Name        : UDT.cpp
// Author      : Rafal
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
#include <udt.h>
#include <arpa/inet.h>
#include <string.h>
#include <sstream>      // std::stringstream


using namespace std;
using namespace UDT;

int main()
{
UDTSOCKET client = UDT::socket(AF_INET, SOCK_STREAM, 0);

sockaddr_in serv_addr;
serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(9000);
inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);

memset(&(serv_addr.sin_zero), '\0', 8);

// connect to the server, implict bind
if (UDT::ERROR == UDT::connect(client, (sockaddr*)&serv_addr, sizeof(serv_addr)))
{
  cout << "connect: " << UDT::getlasterror().getErrorMessage();
  return 0;
}
char* hello = new char[50];

for(int i=48; i<127;i++) {
		for(int x=0;x<50;x++) {
			hello[x] = i;
		}
	stringstream ss;
	ss <<"<"<< i << "|"<< hello<<">";
	cout<<ss.str().c_str();

	if (UDT::ERROR == UDT::send(client, (const char*)ss.str().c_str(), ss.str().size(), 0))
	{
	  cout << "send: " << UDT::getlasterror().getErrorMessage();
	//  return 0;
	}
	//delete hello;
}


UDT::close(client);

return 1;
}
